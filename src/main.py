# pylint: disable=line-too-long
"""The following endpoints in this API returns information about stores, store addresses and specific transactions from the database."""
import uuid
from typing import Optional
from fastapi import FastAPI, HTTPException
import psycopg

app = FastAPI()


@app.on_event("startup")
def startup():
    """This is used to establish the DB connection."""
    app.db = psycopg.connect("postgresql://postgres:DjExUSMcwWpzXziT@doe21-db.grinton.dev/u05")


@app.on_event("shutdown")
def shutdown():
    """This is used to correctly close the DB connection when stopping the API."""
    app.db.close()


@app.get("/stores")
def get_all_stores():
    """This endpoint returns all stores with name, address, zip, city"""
    query = """
        SELECT stores.name, address, zip, city FROM store_addresses
        INNER JOIN stores ON store_addresses.store=stores.id;
        """
    with app.db.cursor() as cur:
        cur.execute(query)
        stores = cur.fetchall()
        result = {"data": [{"name": d[0], "address": f"{d[1]}, {d[2]} {d[3]}"} for d in stores]}
        return result


@app.get("/store/{name}")
async def get_store(name: str):
    """This endpoint returns a specific store's name and address chose by name,
        Returns '404 Item not found' if the given name is not in the DB """
    query = """
        SELECT stores.name, address, zip, city
        FROM store_addresses
        INNER JOIN stores ON store_addresses.store=stores.id
        WHERE stores.name = %s;
        """

    with app.db.cursor() as cur:
        cur.execute(query, (name.title(),))
        stores = cur.fetchall()
        result = {"data": [{"name": d[0], "address": f"{d[1]}, {d[2]} {d[3]}"} for d in stores]}
    if not stores:
        raise HTTPException(status_code=404, detail=f'{name} Not Found')
    return result


@app.get("/cities")
def get_all_cities(zip_nr: Optional[str] = None):
    """The following endpoint returns a specific city name if zip_nr is given, else returns all the unique cities"""
    query = """
        SELECT city,zip
        FROM store_addresses
        ;
        """

    with app.db.cursor() as cur:
        cur.execute(query,)
        cities = cur.fetchall()
        city_list = []
        for item in cities:
            city_list.append(item[0])
            if zip_nr in item:
                city = {"data": [{"city": item[0]}]}
                return city
        distinct_city = sorted(set(city_list))
        distinct_city = {"data": [{"city": d} for d in distinct_city]}
        return distinct_city


@app.get("/sales")
def get_sales():
    """This endpoint returns all the sales in the DB"""
    query = """
        SELECT stores.name, sales.time, sales.id
        FROM stores
        INNER JOIN sales on sales.store=stores.id
        ;
        """

    with app.db.cursor() as cur:
        cur.execute(query)
        sales = cur.fetchall()
        sales = {"data": [{"store": s[0], "timestamp": f"{s[1]}".replace(" ", "T").replace("-", ""), "sale_id": f"{s[2]}"} for s in sales]}
        return sales


@app.get("/sales/{sale_id}")
async def get_sale_id(sale_id: str):
    """This endpoint returns store name, timestamp, sale id, product name and quantity chosen by sale_id
        If the UUID is invalid, return '422 Unprocessable entity',
        If the UUID is valid but not in the DB, return '404 Item not found'"""
    try:
        uuid.UUID(sale_id)
    except ValueError as err:
        raise HTTPException(status_code=422,
                            detail="422 Unprocessable entity") from err
    query = """
        SELECT stores.name, sales.time, sales.id, products.name, sold_products.quantity
        FROM stores
        INNER JOIN sales ON sales.store=stores.id
        INNER JOIN sold_products ON sales.id=sold_products.sale
        INNER JOIN products ON sold_products.product=products.id
        WHERE sales.id=%s;
        """
    with app.db.cursor() as cur:
        cur.execute(query, (sale_id,))
        sales = cur.fetchall()
    if sales:
        name, qty, products = [], [], []
        for item in sales:
            store = item[0]
            time = item[1]
            sale = item[2]
            name.append(item[3])
            qty.append(item[4])
        while len(name) > 0:
            products.append({"name": name.pop(0), "qty": int(qty.pop(0))})
        result = {"data": {"store": store, "timestamp": str(time).replace(" ", "T").replace("-", ""), "sale_id": sale, "products": products}}
        return result
    raise HTTPException(status_code=404, detail="Item not found")
